package main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import Localization.R;

public class InputDestination extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mov_selection);

        this.findViewById(R.id.imageButton2).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent start = new Intent(getApplicationContext(), Navigator.class);
                        start.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(start);
                        //finish();
                    }
                });
    }
}
